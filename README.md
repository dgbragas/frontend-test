# Front-end test - devs.nation

Teste focado no desenvolvimento de uma pequena aplicação onde consumiremos a [API do Github](https://developer.github.com/v3/) para expor algumas informações padrões de um usuário logado.

**URL de consumo:** https://api.github.com/users/[usuario-que-esta-logando]

## Índice

- [Desenvolvimento de telas](#desenvolvimento-de-telas)

  - [Estilos](#estilos)

  - [Tela de login](#login)
  
    - [Durante o login](#durante-o-login)
    
    - [Erro no login](#erro-ao-logar)
    
  - [Tela de perfil](#profile)
  
- [Demais considerações](#demais-considerações)

## Desenvolvimento de telas

A aplicação que você terá de construir girará em torno de duas telas: **Login** e **Profile** - sendo que, para acessar a rota profile, o usuário precisa estar **autenticado**.

Logo abaixo estou anexando o link para o protótipo onde vocês conseguiram visualizar os **espaçamentos**, **cores** e usos da **tipografia**. 

É de extrema importância que vocês acessem e tentem absorver as informações da estrutura que estão lá; isso pois no dia-a-dia vocês sempre terão de dissecar interfaces para construir as demandas requeridas - *e vão por mim, os designers gostam que os espaçamentos estejam exatamente iguais ao layout desenhado*.

**Link do protótipo:** https://www.figma.com/file/E7hBDrBqLIncgmwo92F9Ah/Front-end-test?node-id=0%3A1

**Protótipo interativo:** https://www.figma.com/proto/E7hBDrBqLIncgmwo92F9Ah/Front-end-test?node-id=1%3A2&scaling=min-zoom

## Estilos
Paleta de cores e tipografias à serem utilizadas no desenvolvimento da aplicação.

![Login](/assets/colors-and-typography.png)

### Login
Esta tela consistirá no **ponto inicial do usuário** (rota raíz), é aqui que ele preencherá o campo para conseguir (ou não) acessar a aplicação e visualizar as suas informações.

![Login](/assets/login.png)


#### Durante o login
Observe que o texto existente na tela anterior era apenas um [**placeholder**](https://developer.mozilla.org/en-US/docs/Web/CSS/::placeholder), ou seja, é necessário que vocês **alterem a cor e a opacidade do texto e do próprio placeholder** do input para que este campo fique igual ao layout.

![Login writing](/assets/login-writing.png)


#### Erro ao logar
Caso o usuário digitado seja **inválido** - ou seja, não existir uma conta no Github com este usuário - vocês precisaram **impedir o acesso** dele e mostrar uma mensagem logo abaixo falando **"Usuário não encontrado"**. Até porque convenhamos que o usuário não deseja entrar na aplicação e ver ela quebrando ou não trazendo nenhuma informação, não é mesmo?

![Login error](/assets/login-error.png)


### Profile
Uma vez que o usuário acessar a aplicação, ele será **redirecionado** para uma outra tela (/profile) onde será mostrada todas as informações da conta dele e, ao fim, haverá um botão onde ele irá se deslogar e sair da aplicação de volta a tela inicial.

![View profile](/assets/view-profile.png)


## Demais considerações

Para construir esta aplicação eu particularmente recomendaria que você utilizasse o **framework [ReactJS](https://pt-br.reactjs.org/)** ou relacionados (Angular, Vue, entre outros) por motivos de **facilidade na integração e renderização dos elementos**. Entretanto, sinta-se a vontade para escolher aquilo que estiver mais confortável de utilizar ( até mesmo JavaScript puro :p ).

Ademais o desenvolvimento desta aplicação também poderá conter alguns **extras** caso esteja em busca de um desafio um pouco maior, tais como:

- **Autenticação de rota:** O usuário apenas considerá acessar a rota /profile se estiver logado. Ou seja, se você **digitar /profile na URL** haverá um **redirecionamento** para a rota raíz novamente.

- **Cálculo do campo "NÃO ESTÃO SEGUINDO:"** Está informação **não está diretamente exposta na API** porém há como **calculá-la** com as informações que você já possui (e está utilizando para montar a aplicação).

- **Auto redirecionamento:** Há também a possibilidade de visualizar se o usuário **já havia logado na aplicação** antes e, assim que ele acessar a aplicação, ele ser **redirecionado para a rota "/profile" 'automagicamente'**. Para isso, vocês podem dar uma checada no uso de [localStorage](https://developer.mozilla.org/pt-BR/docs/Web/API/Window/Window.localStorage).

Por fim caso queiram tirar alguma dúvida, verificar se estão no caminho certo ou quaisquer outras abordagens vocês podem entrar em contato comigo. Meu número está no grupo do **WhatsApp** e meu usuário é **@dgbragas** em quaisquer redes sociais que vocês procurarem.

PS.: Reforçando, se atentem aos **espaçamentos**, **cores**, **tamanhos de fonte** e todos estes detalhes, eles são extremamente úteis no dia-a-dia.

Happy hacking! =)
